var totalChance = -1;
var eventProbability = 35;
var currentEvents = [];


function processRemainingEventsTime() {
  for(var i = 0; i<currentEvents.length; i++) {
    var event = currentEvents[i];
    event["time"] -= 1;
  }
  removeExpiredEvents();
}

function removeExpiredEvents() {
  currentEvents = currentEvents.filter(
    function(element) {
      return element["time"] > 0;
    }
  )
}

function randomEvent() {
  if(random(0, 100) < eventProbability) {
    
    var event = copyOf(getRandomElementBasedOnProbability(events));
      
    if(event !== undefined) {
      display(event);
      currentEvents.push(event)
    }
  } else {
    console.log("No event this time...")
  }
}

function display(event) {
    alert(event["name"] + "\n\n" + event["description"])
}

function copyOf(event) {
   var eventTemplate = getRandomElementBasedOnProbability(events)
   var event = {};
      
   event["name"] = eventTemplate["name"]
   event["description"] = eventTemplate["description"]
   event["time"] = eventTemplate["time"]
    
   return event;
}