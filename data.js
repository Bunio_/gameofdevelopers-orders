var maxOrders = 10;
var initialOrders = 5;

var buildings = [
  {
    name : "Domek",
    cost : 10,
    points : 5,
    time : 5,
    probability : 25
  },
  {
    name : "Rezydencja",
    cost : 15,
    points : 10,
    time : 8,
    probability : 25
  },
  {
    name : "Biurowiec",
    cost : 20,
    points : 15,
    time : 10,
    probability : 20
  },
  {
    name : "Hotel",
    cost : 25,
    points : 20,
    time : 13,
    probability : 15
  },
  {
    name : "Drapacz chmur",
    cost : 40,
    points : 40,
    time : 15,
    probability : 10
  },
  {
    name : "Centrum handlowe",
    cost : 50,
    points : 40,
    time : 20,
    probability : 5
  },
]

var extracted1 = {
    name : "Las",
    cost_mod : 10,
    time_mod : 4,
    points_mod : 15,
    probability : 20
  };
var terrains = [
  {
    name : "Działka budowlana",
    cost_mod : 0,
    time_mod : 0,
    points_mod : 0,
    probability : 40
  },
  {
    name : "Łąka",
    cost_mod : 5,
    time_mod : 2,
    points_mod : 10,
    probability : 30
  },
  extracted1
]

var events = [
  {
    name : "Strajk ekoterrorystów",
    description : "Wstrzymanie prac w lesie.",
    probability : 15,
    time: 2
  },
  {
    name : "Niekorzystny biometr",
    description : "Wstrzymanie prac na 1 turę.",
    probability : 10,
    time: 1
  },
  {
    name : "Strasznie gorąco!",
    description : "W tej turze prace posuwają się 2x wolniej.",
    probability : 10,
    time: 1
  },
  {
    name : "Strajk robotników",
    description : "Wstrzymanie prac na 1 turę.",
    probability : 10,
    time: 1
  },
  {
    name : "500+ na każdy młotek!",
    description : "Ale super! W tej turze prace posuwają się 2x szybciej.",
    probability : 5,
    time: 1
  },
  {
    name : "Silne wiatry",
    description : "Wstrzymane prace na niezalesionych terenach.",
    probability : 5,
    time: 1
  }
]
