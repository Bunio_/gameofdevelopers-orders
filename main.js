$(document).ready(function () {
    addInitialOrders();
    $("#nextTurnBtn").click(function () {
        nextTurn();
    });
});

function addInitialOrders() {
    for (var i = 0; i < initialOrders; i++) {
        addNewOrder()
    }
    sortOrders();
    refreshTables()
}


function nextTurn() {
    processRemainingOrdersTime();
    processRemainingEventsTime();

    if (currentOrders.length < maxOrders) {
        addNewOrder();
    }

    randomEvent();
    sortOrders();
    refreshTables()
}

function refreshTables() {
    refreshEvents();
    refreshOrders();
    refreshListeners();
}

function refreshOrders() {

    var ordersTable = $("#ordersTable");
    $("#ordersTable tbody").empty();

    for (var i = 0; i < currentOrders.length; i++) {
        var order = currentOrders[i];
        var row = "<tr>";
        row += "<td>" + order["name"] + "</td>"
        row += "<td>" + order["terrain"] + "</td>"
        row += "<td>" + order["cost"] + "</td>"
        row += "<td>" + order["points"] + "</td>"
        row += "<td>" + order["time"] + "</td>"
        row += "<td> <button id=" + i + " class=\"btn btn-success btn-order-ready\">✔</button></td>"
        row += "</tr>"
        ordersTable.append(row);
    }
}

function refreshEvents() {

    var eventsTable = $("#eventsTable");
    $("#eventsTable tbody").empty();

    for (var i = 0; i < currentEvents.length; i++) {
        var event = currentEvents[i];
        var row = "<tr>";
        row += "<td>" + event["name"] + "</td>"
        row += "<td>" + event["description"] + "</td>"
        row += "<td>" + event["time"] + "</td>"
        row += "</tr>"
        eventsTable.append(row);
    }
}

function refreshListeners() {
    $('.btn-order-ready').click(function (btn) {
        var order = currentOrders[btn.target.id];

        if (confirm("Czy na pewno ukończono budowę: " + order["name"] + " (" + order["terrain"] + ") ?")) {
            alert("Gratulację! Zdobyłeś " + order["points"] + " punktów!");
            currentOrders.splice(btn.target.id, 1)
            refreshTable()
        }
    });

}
