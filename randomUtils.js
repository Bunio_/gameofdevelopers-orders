function randomElementOf(array) {
    return array[random(0, array.length)]
}

function random(min, max) {
    return Math.floor(Math.random() * max) + min;
}


function getRandomElementBasedOnProbability(elements) {
    var totalChance = calculateTotalChance(elements)
    var randomNumber = random(0, totalChance);
    var currentProbability = 0;

    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        var elementProbability = element["probability"];

        if (randomNumber <= currentProbability + elementProbability) {
            return element;
        } else {
            currentProbability += elementProbability;
        }
    }
}

function calculateTotalChance(elements) {
    var totalChance = 0;

    elements.forEach(
        function (element) {
            totalChance += element["probability"]
        }
    )

    return totalChance;
}
