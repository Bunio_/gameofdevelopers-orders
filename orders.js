var currentOrders = [];

function processRemainingOrdersTime() {
    for (var i = 0; i < currentOrders.length; i++) {
        var order = currentOrders[i];
        order["time"] -= 1;

        if (order["time"] === 0) {
            alert("Zlecenie na " + order["name"] + " (" + order["terrain"] + ") wygasło.")
        }
    }
    removeExpiredOrders();
}

function addNewOrder() {
    var newOrder = getRandomOrder();
    currentOrders.push(newOrder);
}

function getRandomOrder() {
    var building = getRandomElementBasedOnProbability(buildings)
    var terrain = getRandomElementBasedOnProbability(terrains)
    var order = createOrder(building, terrain);
    return order;
}

function createOrder(building, terrain) {
    var order = {};
    order["name"] = building["name"]
    order["terrain"] = terrain["name"]
    order["cost"] = building["cost"] + terrain["cost_mod"]
    order["points"] = building["points"] + terrain["points_mod"]
    order["time"] = building["time"] + terrain["time_mod"]
    return order;
}

function removeExpiredOrders() {
    currentOrders = currentOrders.filter(
        function (element) {
            return element["time"] > 0;
        }
    )
}

function sortOrders() {
    currentOrders.sort(
        function (a, b) {
            return a["time"] - b["time"]
        }
    )
}
